#include <stdio.h>
#include <stdlib.h>

#include "bitmap.c"

#define XSIZE 2560
#define YSIZE 2048

#define XSCALE 2
#define YSCALE 2

#define INVERT_RED 1
#define INVERT_GREEN 1
#define INVERT_BLUE 0

typedef unsigned char uchar;
uchar *scale(uchar *image, int *x, int *y, int xscale, int yscale);
uchar *invert_color(uchar *image, int xsize, int ysize, int red, int green, int blue);

int main() {

  int xsize = XSIZE;
  int ysize = YSIZE;

  printf("Hello!\n");
  uchar *image = calloc(xsize * ysize * 3, 1); // Three uchars per pixel (RGB)

  readbmp("before.bmp", image);

  image = scale(image, &xsize, &ysize, XSCALE, YSCALE);
  image = invert_color(image, xsize, xsize, INVERT_RED, INVERT_GREEN, INVERT_BLUE);

  savebmp("after.bmp", image, xsize, ysize);

  return 0;
}

uchar *scale(uchar *image, int *x, int *y, int xscale, int yscale) {
  uchar *newImage = calloc(*x * xscale * *y * yscale * 3, 1);

  int index;

  for (int i = 0; i < *x; ++i) {
    for (int j = 0; j < *y; ++j) {
      for (int k = 0; k < 3; ++k) {
        index = (i + j * *x) * 3 + k;

        for (int a = 0; a < xscale; ++a) {
          for (int b = 0; b < yscale; ++b) {
            newImage[(i * xscale + a + (j * yscale + b) * *x * xscale) * 3 + k] = image[index];
          }
        }
      }
    }
  }

  printf("SCALED\n");
  *x = *x * xscale;
  *y = *y * yscale;

  return newImage;
}

uchar *invert_color(uchar *image, int xsize, int ysize, int red, int green, int blue) {
  for (int i = 0; i < xsize; ++i) {
    for (int j = 0; j < ysize; ++j) {
      for (int k = 0; k < 3; ++k) {
        if ((red && k == 0) || (green && k == 1) || (blue && k == 2)) {
          image[(i + j * xsize) * 3 + k] ^= 0xFF;
        }
      }
    }
  }
  if (red)
    printf("Inverted red.\n");
  if (green)
    printf("Inverted green.\n");
  if (blue)
    printf("Inverted blue.\n");

  return image;
}
