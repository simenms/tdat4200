#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#include "bitmap.c"

#define XSIZE 2560
#define YSIZE 2048

#define XSCALE 2
#define YSCALE 2

#define INVERT_RED 1
#define INVERT_GREEN 1
#define INVERT_BLUE 1

typedef unsigned char uchar;
uchar *scale(uchar *image, int *x, int *y, int xscale, int yscale);
uchar *invert_color(uchar *image, int xsize, int ysize, int red, int green, int blue);

int main(int argc, char **argv) {
  // Initialize
  MPI_Init(NULL, NULL);

  // Fetch world size and world rank
  int world_size;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  uchar *image;
  int xsize = XSIZE;
  int ysize = YSIZE;

  if (world_rank == 0) {
    printf("Process %d says hello!\n", world_rank);
    image = calloc(xsize * ysize * 3, 1); // Three uchars per pixel (RGB)

    readbmp("before.bmp", image);
    image = scale(image, &xsize, &ysize, XSCALE, YSCALE);
    image = invert_color(image, xsize, ysize, INVERT_RED, INVERT_GREEN, INVERT_BLUE);
    //ysize = ysize/world_size;
    // send image parts to all threads

    int to_be_received;
    MPI_Status *stat;

    MPI_Recv(&to_be_received, 1, MPI_INT, 1, 4, MPI_COMM_WORLD, stat);
    printf("Hello from process %d, the received int is %d\n", world_rank, to_be_received);
    printf("Hello from process %d, the address of the received int is %d\n", world_rank, &to_be_received);
    MPI_Recv(&to_be_received, 1, MPI_INT, 1, 4, MPI_COMM_WORLD, stat);
    printf("Hello from process %d, the received int is %d\n", world_rank, to_be_received);
    printf("Hello from process %d, the address of the received int is %d\n", world_rank, &to_be_received);
  }

  else {

    int new_int = 232;
    printf("Hello from process %d, the address of the sent int is %d\n", world_rank, &new_int);
    MPI_Send(&new_int, 1, MPI_INT, 0, 4, MPI_COMM_WORLD);

    ++new_int;
    MPI_Send(&new_int, 1, MPI_INT, 0, 4, MPI_COMM_WORLD);
    // printf("Hello from process %d, the received int is %d\n", world_rank, to_be_received);
    printf("Hello from process %d, the address of the sent int is %d\n", world_rank, &new_int);
  }

  if (world_rank == 0) {
    //receive image
    savebmp("after.bmp", image, xsize, ysize);
    free(image);
  }
  //else {
  //   // send image;
  // }

  // free(image);
  MPI_Finalize();
}

uchar *scale(uchar *image, int *x, int *y, int xscale, int yscale) {
  uchar *newImage = calloc(*x * xscale * *y * yscale * 3, 1);

  int index;

  for (int i = 0; i < *x; ++i) {
    for (int j = 0; j < *y; ++j) {
      for (int k = 0; k < 3; ++k) {
        index = (i + j * *x) * 3 + k;

        for (int a = 0; a < xscale; ++a) {
          for (int b = 0; b < yscale; ++b) {
            newImage[(i * xscale + a + (j * yscale + b) * *x * xscale) * 3 + k] = image[index];
          }
        }
      }
    }
  }

  printf("SCALED\n");
  *x = *x * xscale;
  *y = *y * yscale;

  return newImage;
}

uchar *invert_color(uchar *image, int xsize, int ysize, int red, int green, int blue) {
  // for (int i = 0; i < xsize; ++i) {
  //   for (int j = 0; j < ysize; ++j) {
  //     for (int k = 0; k < 3; ++k) {
  //       if ((red && k == 0) || (green && k == 1) || (blue && k == 2)) {
  //         // if (i == xsize - 1 && j == ysize - 1)
  //           // printf("%0x\n", image + (i + j * xsize) * 3 + k);
  //         *(image + (i + j * xsize) * 3 + k) ^= 0xFF;
  //       }
  //     }
  //   }
  // }
  for (int i = 0; i < xsize; ++i) {
    for (int j = 0; j < ysize; ++j) {
      for (int k = 0; k < 3; ++k) {
        if ((red && k == 0) || (green && k == 1) || (blue && k == 2)) {
          // if (i == xsize - 1 && j == ysize - 1)
          // printf("%0x\n", image + (i + j * xsize) * 3 + k);
          *(image + (i + j * xsize) * 3 + k) ^= 0xFF;
        }
      }
    }
  }
  // if (red)
  //   printf("Inverted red.\n");
  // if (green)
  //   printf("Inverted green.\n");
  // if (blue)
  //   printf("Inverted blue.\n");

  return image;
}

